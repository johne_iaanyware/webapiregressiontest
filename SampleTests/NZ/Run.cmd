@echo off
rem Just Summary and Create log in Log folder
..\..\WebApiTest\bin\Debug\WebApiTest.exe --generate --CreateLogFile --Logdir Logs --loglevel 128 %* --Test %CD%

rem Just Error and Summary and Create log in Log folder
rem ..\..\WebApiTest\bin\Debug\WebApiTest.exe --generate --CreateLogFile --Logdir Logs --loglevel 134 %* --Test %CD%

rem no log but errors and summary to console
rem ..\..\WebApiTest\bin\Debug\WebApiTest.exe --generate --logtarget 0 --loglevel 134 %* --Test %CD%

rem for no summary or log output just errorlevel set:
rem ..\..\WebApiTest\bin\Debug\WebApiTest.exe --generate --logtarget 0 --loglevel 0 %* --Test %CD%
echo Errorlevel is %ERRORLEVEL%
