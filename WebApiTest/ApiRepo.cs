﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Ia.Common.Models.Exceptions;
using Ia.Reports.Models;
using Ia.Reports.Models.Distribution;
using Ia.Reports.Models.Reports;
using Ia.Reports.Models.Reports.DocPacks;

namespace WebApiTest
{

    public class ApiRepo
    {
        private static Timer _refreshTokenTimer = null;

        public static string Token { get; private set; }
        public static int TokenExpiresIn { get; private set; }
        public static string RefreshToken { get; private set; }
        private static Param Arg => Parameter.Instance;
        private static string AppClient = Arg.AppClient.ToString();
        private static string JwtClientId = "iadviser-desktop";
        private static string JwtClientSecretKey = "2OnluiuNCUWomJ8elHrWm1+GBVDWWAq+vUyD5FQzT6M=";
        public string BaseUrl;

        internal async Task<TResponse> PostAsync<TRequest, TResponse>(string uri, TRequest request, string callingMethodName)
        {
            Log.Trace();
            await ApiRepo.GetToken();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ApiRepo.Token);

                var response = await client.PostAsync(uri, request, new JsonMediaTypeFormatter());

                if (!response.IsSuccessStatusCode)
                {
                    var exceptionDetail = await response.Content.ReadAsAsync<DbExceptionRow>();
                    var reasonPhraseExtended = GetExtendedReasonPhrase(response.ReasonPhrase, exceptionDetail);
                    throw new Exception(
                        String.Format(callingMethodName + ": IsSuccessStatusCode == {0}. StatusCode = {1}. ReasonPhrase = {2}",
                            response.IsSuccessStatusCode, response.StatusCode, reasonPhraseExtended));
                }

                return await response.Content.ReadAsAsync<TResponse>();
            }
        }
        private static async Task PostAsync<TRequest>(string uri, TRequest request, string callingMethodName)
        {
            if (string.IsNullOrEmpty(Token)) await GetToken();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Settings.DocumentApiBaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                var response = await client.PostAsync(uri, request, new JsonMediaTypeFormatter());

                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    // refresh the token and try again
                    await GetToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    response = await client.PostAsync(uri, request, new JsonMediaTypeFormatter());
                }

                if (!response.IsSuccessStatusCode)
                {
                    var exceptionDetail = await response.Content.ReadAsAsync<DbExceptionRow>();
                    var reasonPhraseExtended = GetExtendedReasonPhrase(response.ReasonPhrase, exceptionDetail);
                    throw new Exception(
                        string.Format(callingMethodName + ": IsSuccessStatusCode == {0}. StatusCode = {1}. ReasonPhrase = {2}",
                            response.IsSuccessStatusCode, response.StatusCode, reasonPhraseExtended));
                }
            }
        }

        private static object GetExtendedReasonPhrase(string reasonPhrase, DbExceptionRow exceptionDetail)
        {
            if (exceptionDetail != null)
            {
                var extendedReasonPhrase = new StringBuilder();
                extendedReasonPhrase.AppendLine(reasonPhrase);
                if (!String.IsNullOrEmpty(exceptionDetail.Message))
                {
                    extendedReasonPhrase.AppendLine("Message: " + exceptionDetail.Message);
                }
                if (!String.IsNullOrEmpty(exceptionDetail.ExceptionMsg))
                {
                    extendedReasonPhrase.AppendLine("ExceptionMsg: " + exceptionDetail.ExceptionMsg);
                }
                if (!String.IsNullOrEmpty(exceptionDetail.ExceptionSource))
                {
                    extendedReasonPhrase.AppendLine("ExceptionSource: " + exceptionDetail.ExceptionSource);
                }
                if (!String.IsNullOrEmpty(exceptionDetail.ExceptionType))
                {
                    extendedReasonPhrase.AppendLine("ExceptionType: " + exceptionDetail.ExceptionType);
                }
                if (!String.IsNullOrEmpty(exceptionDetail.ExceptionUrl))
                {
                    extendedReasonPhrase.AppendLine("ExceptionUrl: " + exceptionDetail.ExceptionUrl);
                }
                if (!String.IsNullOrEmpty(exceptionDetail.StackTrace))
                {
                    extendedReasonPhrase.AppendLine("StackTrace: " + exceptionDetail.StackTrace);
                }
                return extendedReasonPhrase.ToString();
            }
            else
                return reasonPhrase;
        }

        internal async Task<string> GetJson(string url)
        {
            await ApiRepo.GetToken();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("AppClient", Arg.AppClient.ToString());

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ApiRepo.Token);

                var response = await client.GetAsync(url);


                if (!response.IsSuccessStatusCode)
                {
                    var exceptionDetail = await response.Content.ReadAsAsync<DbExceptionRow>();
                    var reasonPhraseExtended = GetExtendedReasonPhrase(response.ReasonPhrase, exceptionDetail);
                    throw new Exception(
                        String.Format(" IsSuccessStatusCode == {0}. StatusCode = {1}. ReasonPhrase = {2}",
                            response.IsSuccessStatusCode, response.StatusCode, reasonPhraseExtended));
                }

                return await response.Content.ReadAsStringAsync();
            }
        }
        internal static async Task GetToken()
        {
            //  if (Arg.HostName == Parameter.HostDefault) return;
            if (String.IsNullOrEmpty(Arg.User) || !String.IsNullOrEmpty(Token)) return;
            string url = $"{Arg.Host}";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Add("AppClient", AppClient);
                var pwsha = Arg.Pwd;//Parameter.Instance.HostName=="live" ? $"sha512:{Arg.Pwd}" : $"{Arg.Pwd}";
                var contArr = new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", $"{Arg.User}@iaanyware.net"),
                    new KeyValuePair<string, string>("password", pwsha),
                    new KeyValuePair<string, string>("password_hashed", "true"),
                    new KeyValuePair<string, string>("app_client", AppClient),
                    new KeyValuePair<string, string>("client_id", JwtClientId),
                    new KeyValuePair<string, string>("client_secret", JwtClientSecretKey),
                    new KeyValuePair<string, string>("user_type", "adviser"),
                    new KeyValuePair<string, string>("tenant", "IAA"),
                    new KeyValuePair<string, string>("build_environment", Arg.CompareTo=="prelive"?"stage":Arg.CompareTo),
                };
                var content = new FormUrlEncodedContent(contArr);
                var response = await client.PostAsync($"{url}/token", content);
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(url);
                    foreach (var c in contArr) Console.WriteLine($"{c.Key} = {c.Value}");
                    throw new Exception($"GetToken: IsSuccessStatusCode == {response.IsSuccessStatusCode}. StatusCode = {response.StatusCode}. ReasonPhrase = {response.ReasonPhrase}");
                }
                var resultItem = await response.Content.ReadAsAsync<dynamic>();
                Token = resultItem.access_token.ToString();
            }
        }

        public static async Task SendRefreshToken()
        {
            if (string.IsNullOrEmpty(RefreshToken))
            {
                // we don't have a refresh token so just send through the user's details and get a new token
                await GetToken();
            }
            else
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Settings.DocumentApiBaseUri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("AppClient", AppClient);

                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("grant_type", "refresh_token"),
                        new KeyValuePair<string, string>("client_id", JwtClientId),
                        new KeyValuePair<string, string>("client_secret", JwtClientSecretKey),
                        new KeyValuePair<string, string>("refresh_token", RefreshToken)
                    });

                    HttpResponseMessage response = null;
                    try
                    {
                        response = await client.PostAsync(Settings.DocumentApiBaseUri + "/token", content);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("SendRefreshToken: Unexpected - token failure! " + ex.Message, ex); // should never happen!
                    }


                    if (!response.IsSuccessStatusCode)
                    {
                        // get a new token as the refresh token has failed
                        await GetToken();
                    }
                    else
                    {
                        var resultItem = await response.Content.ReadAsAsync<dynamic>();
                        SetTokenDetails(resultItem.access_token.ToString(), (int)resultItem.expires_in, resultItem.refresh_token.ToString());
                        if (string.IsNullOrEmpty(Token))
                            throw new Exception("SendRefreshToken: Unexpected - token returned is empty!"); // should never happen!
                    }
                }
            }
        }

        public static void SetTokenDetails(string token, int tokenExpiresIn, string refreshToken)
        {
            Token = token;
            TokenExpiresIn = tokenExpiresIn;
            RefreshToken = refreshToken;

            // set the timer to refresh the token just prior to expiry
            if (_refreshTokenTimer != null)
            {
                _refreshTokenTimer.Dispose();
                _refreshTokenTimer = null;
            }
            _refreshTokenTimer = new Timer
            {
                // tokenExpiresIn is in seconds. Set to expire one minute before but no more frequently than every 5 minutes
                Interval = Math.Max((tokenExpiresIn - 60) * 1000, 300000)
            };
            _refreshTokenTimer.Elapsed += async (sender, args) =>
            {
                _refreshTokenTimer.Stop();
                await SendRefreshToken();
            };
            _refreshTokenTimer.Start();
        }

        public async Task<DocPackGenerationStatus> GetDocPackStatus(Guid docPackGuid)
        {
            var url = String.Format("au/docpack/generation/{0}/status", docPackGuid);
            Log.Trace(url);
            try
            {
                return await GetAsync<DocPackGenerationStatus>(url, "GetDocPackStatus");
            }
            catch
            {
                // ignore - will retry below
            }
            // retry
            return await GetAsync<DocPackGenerationStatus>(url, "GetDocPackStatus");
        }

        public async Task<IEnumerable<DocPackGenerationNotification>> GetDocPackReportDocs(Guid docPackGuid)
        {
            var url = string.Format("au/docpack/generation/{0}/reports", docPackGuid);
            Log.Trace(url);
            try
            {
                return await GetAsync<IEnumerable<DocPackGenerationNotification>>(url, "GetDocPackReportDocs");
            }
            catch
            {
                // ignore - will retry below
            }
            // retry
            return await GetAsync<IEnumerable<DocPackGenerationNotification>>(url, "GetDocPackReportDocs");
        }

        public async Task<IEnumerable<ReportGenerationStatus>> GetDocPackReportItemsStatuses(Guid docPackGuid)
        {
            var url = string.Format("au/docpack/generation/{0}/reports/status", docPackGuid);
            Log.Trace(url);
            try
            {
                return await GetAsync<IEnumerable<ReportGenerationStatus>>(url, "GetDocPackReportItemsStatuses");
            }
            catch
            {
                // ignore - will retry below
            }
            // retry
            return await GetAsync<IEnumerable<ReportGenerationStatus>>(url, "GetDocPackReportItemsStatuses");
        }

        public async Task<byte[]> GetDocPackPdfAsync(Guid criteriaId, Guid documentGuid)
        {
            var url = string.Format("au/docpack/view/{0}/{1}", criteriaId, documentGuid);
            Log.Trace(url);
            return await GetAsByteArrayAsync(url, "GetDocPackPdfAsync");
        }

        internal static async Task<byte[]> GetAsByteArrayAsync(string uri, string callingMethodName)
        {
            if (string.IsNullOrEmpty(Token)) await GetToken();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Settings.DocumentApiBaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                var response = await client.GetAsync(uri);

                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    // refresh the token and try again
                    await GetToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    response = await client.GetAsync(uri);
                }

                if (!response.IsSuccessStatusCode)
                {
                    var exceptionDetail = await response.Content.ReadAsAsync<DbExceptionRow>();
                    var reasonPhraseExtended = GetExtendedReasonPhrase(response.ReasonPhrase, exceptionDetail);
                    throw new Exception(
                        string.Format(callingMethodName + ": IsSuccessStatusCode == {0}. StatusCode = {1}. ReasonPhrase = {2}",
                            response.IsSuccessStatusCode, response.StatusCode, reasonPhraseExtended));
                }

                return await response.Content.ReadAsByteArrayAsync();
            }
        }

        //public async Task<IEnumerable<DistributionHistoryRowItem>> GetDistributionDocUrls(int distributionHistoryId, PolicyType policyType, bool combinedDistribution)
        //{
        //    var url = string.Format("au/docpack/distribution/downloadurls/{0}/{1}/{2}", distributionHistoryId, policyType, combinedDistribution);
        //    Log.Trace(url);
        //    return await GetAsync<IEnumerable<DistributionHistoryRowItem>>(url, "GetDistributionDocUrls");
        //}

        public async Task<string> GetDistributionDocUrl(string partitionKey, string rowKey)
        {
            var url = string.Format("au/docpack/distribution/downloadurl/{0}/{1}", partitionKey, rowKey);
            Log.Trace(url);
            return await GetAsync<string>(url, "GetDistributionDocUrl");
        }

        //public async Task<string> GetEditHashFromCriteria(PolicyType policyType, int[] entityIds, int docPackId, int reportId, int clientId)
        //{
        //    var quoteIdsParam = string.Empty;
        //    foreach (var entityId in entityIds)
        //    {
        //        quoteIdsParam += string.Format("&quoteId={0}", entityId);
        //    }
        //    var url = string.Format("au/docpack/edit/getedithash?policyType={0}{1}&docPackId={2}&reportId={3}&clientId={4}", policyType, quoteIdsParam, docPackId, reportId, clientId);
        //    Log.Trace(url);
        //    return await GetAsync<string>(url, "GetEditHashFromCriteria");
        //}

        public async Task<string> GetEditableDocument(string editHash)
        {
            var url = string.Format("au/docpack/edit/{0}", editHash);
            Log.Trace(url);
            return await GetAsync<string>(url, "GetEditableDocument");
        }

        public async Task PostEditableContent(string editHash, EditableDocumentDto editableDocumentDto)
        {
            var url = string.Format("au/docpack/edit/{0}", editHash);
            Log.Trace(url);
            await PostAsync<EditableDocumentDto>(url, editableDocumentDto, "PostEditableContent");
        }

        public async Task<string> ResetEditableContent(string editHash)
        {
            var url = string.Format("au/docpack/edit/reset/{0}", editHash);
            Log.Trace(url);
            return await PostAsync<string, string>(url, url, "ResetEditableContent");
        }

        //public async Task LogRedistribution(int distHistoryId, PolicyType policyType, DistributionType distributionType, bool combinedDistribution, bool historyIdIsTheCombinedId)
        //{
        //    var url = string.Format("au/docpack/distribution/redistribute/{0}/{1}/{2}/{3}/{4}", distHistoryId, policyType, distributionType, combinedDistribution, historyIdIsTheCombinedId);
        //    await PostAsync<string>(url, url, "LogRedistribution");
        //}

        public async Task<Dictionary<Guid, string>> GetCacheDocumentList(Guid docPackCriteriaId)
        {
            var url = string.Format("au/docpack/cacheddocs/{0}", docPackCriteriaId);
            Log.Trace(url);
            return await GetAsync<Dictionary<Guid, string>>(url, "GetCacheDocumentList");
        }

        public async Task<Dictionary<string, string>> GetCachedHtml(Guid docPackCriteriaId)
        {
            Log.Trace();
            var url = $"au/docpack/cacheddocs/{docPackCriteriaId}/html"; //TODO  This is ONLY IN DEVELOP Branch!!!!!!!!!!!!!
            Log.Trace(url);
            return await GetAsync<Dictionary<string, string>>(url, "GetCachedHtml");
        }

        private static async Task<TResponse> GetAsync<TResponse>(string uri, string callingMethodName)
        {
            if (String.IsNullOrEmpty(Token)) await GetToken();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Settings.DocumentApiBaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                var response = await client.GetAsync(uri);

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    // refresh the token and try again
                    await GetToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    response = await client.GetAsync(uri);
                }

                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NotFound) throw new Exception($"{response.ReasonPhrase} {uri}");
                    var exceptionDetail = await response.Content.ReadAsAsync<DbExceptionRow>();
                    // var reasonPhraseExtended = GetExtendedReasonPhrase(response.ReasonPhrase, exceptionDetail);
                    throw new Exception(
                        String.Format(callingMethodName + ": IsSuccessStatusCode == {0}. StatusCode = {1}. ReasonPhrase = {2}",
                            response.IsSuccessStatusCode, response.StatusCode, exceptionDetail.Message));
                }

                return await response.Content.ReadAsAsync<TResponse>();
            }
        }

        public async Task<DocPackDistributionSessionModel> CreateDistributionSession(Guid docPackCriteriaId, int distributionType, int recipientPartyId)
        {
            Log.Trace();
            var url = string.Format("au/docpack/distribution/session/{0}/{1}/{2}", docPackCriteriaId, distributionType, recipientPartyId);
            Log.Trace(url);
            return await PostAsync<string, DocPackDistributionSessionModel>(url, url, "CreateDistributionSession");
        }

        public static class Settings
        {
            public static string DocumentApiBaseUri => $"{Arg.Host}";
            public static int MaxDocumentStatusPollingPeriodMinutes => 2;
            public static int DocumentStatusPollingIntervalMS => 1000;
        }


    }

}
