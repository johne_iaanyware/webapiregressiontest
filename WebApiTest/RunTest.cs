﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DiffPlex;
using DiffPlex.DiffBuilder.Model;
using EO.Pdf;
using Fclp.Internals.Extensions;
using Ia.Domain.Models.Enums;
using Ia.Reports.Models.Interfaces;
using Ia.Reports.Models.Report;
using Ia.Reports.Models.Reports;
using Ia.Reports.Models.Reports.DocPacks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp.Extensions;
using RestSharp.Validation;
using Slack.Webhooks;
//using DocPackGenerationStatus = iAdviserApi.ApiModels.Reports.DocPacks.Dtos.DocPackGenerationStatus;
//using iAdviserApi.ApiModels.Interfaces;
//using iAdviserApi.ApiModels.Reports.DocPacks;
//using Ia.Common.Extensions.Testing;
//using Ia.Domain.Enums;
//using Ia.Domain.Models.Report;

namespace WebApiTest
{

    public class Tester
    {
        private static Param Arg => Parameter.Instance;
        private string _projBasePath;
        private string _clientResultsPath;
        private string _criteriaJsonPath;
        private string _htmlResultsPath;
        private string _jsonFilesPath;
        //private string _expectedFileName;
        private readonly int _chContext = 8;
        private string[] dtstr = { @"d/MM/yyyy ", "dd MMM yyyy", "dd MMMM yyyy", @"dd/MM/yyyy" };
        private string[] dtreg = { @"\d+:\d+:\d+ [APap][mM]", "", "", "" };
        private int chContext = 8;

        private ApiRepo _apiRepo;
        private IList<string> _cntFails = new List<string>();
        private IList<string> _cntDiffDocs = new List<string>();
        private IList<string> _cntPass = new List<string>();
        private Stopwatch _tm;
        private string _testResultsPath;
        //private Dictionary<int, SprocModel> _sprocMap;

        public string TestName => string.IsNullOrEmpty(Parameter.Instance.Test) ? _clientResultsPath : Parameter.Instance.Test;

        public AppClientEnum Appclient { get; set; }

        public Tester(AppClientEnum appClient)
        {
            Appclient = appClient;
            _apiRepo = new ApiRepo();
            _apiRepo.BaseUrl = Arg.Host;
            _tm = new Stopwatch();
        }

        public async Task GetReportsWithActiveCriteria()
        {
            //GET 
            var getUlr = $"{Arg.Host}/{Appclient}/reports/testing/getreportswithcriteria";
            var json = await _apiRepo.GetJson(getUlr);
            Log.Debug(json);
            File.AppendAllText(_criteriaJsonPath, json);
        }

        private async Task CreatePaths()
        {
            if (string.IsNullOrEmpty(Arg.Results))
            {
                _projBasePath = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName;
                _testResultsPath = Path.Combine(_projBasePath, @"..\TestResults");
            }
            else
            {
                _testResultsPath = Arg.Results;
            }
            if (!Directory.Exists(_testResultsPath)) Directory.CreateDirectory(_testResultsPath);
            _clientResultsPath = Path.Combine(_testResultsPath, Appclient.ToString());
            if (!Directory.Exists(_clientResultsPath)) Directory.CreateDirectory(_clientResultsPath);

            if (!string.IsNullOrEmpty(Arg.Test))
            {
                if (Directory.Exists(Arg.Test))
                {
                    _clientResultsPath = Arg.Test;
                }

                else
                {
                    var dpPath = Path.Combine(_clientResultsPath, Arg.Test);
                    if (!Directory.Exists(dpPath)) throw new Exception($"Path {dpPath} does not exist ");
                    _clientResultsPath = dpPath;
                }
            }

            _htmlResultsPath = Path.Combine(_clientResultsPath, "html");
            if (!Directory.Exists(_htmlResultsPath)) Directory.CreateDirectory(_htmlResultsPath);
            _criteriaJsonPath = Path.Combine(_clientResultsPath, "Criteria.json");
            _jsonFilesPath = Path.Combine(_clientResultsPath, "json");
            if (!Directory.Exists(_jsonFilesPath)) Directory.CreateDirectory(_jsonFilesPath);
            if (!File.Exists(_criteriaJsonPath) && string.IsNullOrEmpty(Arg.Host)) await GetReportsWithActiveCriteria(); //TODO only working in DEV
        }

        public async Task<bool> HtmlTests()
        {
            await CreatePaths();
            Log.Html($"\n{DateTime.Now.ToString("T")}: Starting Report Regression Test for {_clientResultsPath}", LogLevel.Info);
            var testData = new List<string>();
            if (File.Exists(_criteriaJsonPath))
            {
                var testCritJson = File.ReadAllText(_criteriaJsonPath);
                testData = JsonConvert.DeserializeObject<List<string>>(testCritJson);
            }
            else testData = new List<string>(Directory.GetFiles(_jsonFilesPath, "*.json", SearchOption.TopDirectoryOnly).Select(Path.GetFileNameWithoutExtension));

            var tests = new List<string>();
            if (!string.IsNullOrEmpty(Parameter.Instance.File))
            {
                tests.Add(Parameter.Instance.File);
            }
            else
            {
                var query = testData.Skip(Arg.Skip).Take(Arg.Take);
                tests = query.ToList();
            }
            // if (Parameter.Instance.Csv) LoadSprocFile();
            var testNo = Arg.Skip;
            await Task.WhenAll(tests.Select(i => TestReport(testNo++, i)));
            Log.Html(TestSummary(tests.Count), LogLevel.Summary);
            return (_cntFails.Count == 0 && _cntDiffDocs.Count == 0);
        }

        private async Task TestReport(int testNo, string item)
        {
            ReportCriteria reportCriteria = null;
            try
            {
                _tm.Reset();
                _tm.Start();
                var saveas = string.IsNullOrWhiteSpace(Parameter.Instance.SaveAs) ? Parameter.Instance.HostName : Parameter.Instance.SaveAs;
                var itemCritJsonPath = GetLatestFile($"{_jsonFilesPath}", new[] { $@"{item}.{saveas}.json", $@"{item}.json" });
                //var itemCritJsonPath = Path.Combine(_jsonFilesPath, item + ".json");
                string critJson;
                if (File.Exists(itemCritJsonPath)) critJson = File.ReadAllText(itemCritJsonPath);
                else throw new Exception($"{itemCritJsonPath} json file not found");

                if (string.IsNullOrEmpty(critJson) || critJson == "null") throw new Exception($"No File Content for: {itemCritJsonPath}");
                reportCriteria = JsonConvert.DeserializeObject<ReportCriteria>(critJson);
                if (string.IsNullOrEmpty(reportCriteria.ReportSystemKey)) reportCriteria.ReportSystemKey = item;

                var results = "";
                var csv = "";
                int csvDiffs = 0;
                Guid repStorGuid = new Guid();
                var resultsPath = Path.Combine(_htmlResultsPath, item);
                if (!Directory.Exists(resultsPath)) Directory.CreateDirectory(resultsPath);
                var htmlFilePath = Path.Combine(resultsPath, $@"{item}.{saveas}.html");
                if (Parameter.Instance.Generate || !File.Exists(htmlFilePath) || Parameter.Instance.Pdf)
                {
                    repStorGuid = await QueueReport(reportCriteria);
                    await WaitForReportGeneration(repStorGuid);
                    results = await GetReportHtml(repStorGuid);
                    if (Parameter.Instance.Pdf) await GetSaveReportPdf(repStorGuid, item);
                    if (Parameter.Instance.Csv)
                    {
                        csv = await GetReportCsv(repStorGuid);
                        csvDiffs = CompareCsv(csv, item);
                        if (csvDiffs > 0)
                        {
                            Log.Error($"Test {testNo,3} Failed {item} ReportId: {reportCriteria.ReportId} - {csvDiffs} CSV Differences Found");
                            Log.Html($"Test {testNo,3} Failed ReportId: {reportCriteria.ReportId} - {csvDiffs} {item.ToDiffLinkHtml()} found between {item.ToHostLinkHtml()} and {item.ToDiffLinkCompareHtml()} ");
                            _cntDiffDocs.Add(item);
                        }
                    }

                    if (string.IsNullOrEmpty(results))
                    {
                        throw new Exception("No HTML Content");
                    }
                    _tm.Stop();
                    results = RemoveKnownHtmlChanges(results);
                }
                else
                {
                    results = File.ReadAllText(htmlFilePath);
                }
                var differences = CompareHtml(results, item, resultsPath);
                if (differences + csvDiffs == 0)
                {
                    Log.Info($"Test {testNo,3} Passed {item}");
                    Log.Html($"Test {testNo,3} Passed <br/>{item.ToHostLinkHtml()} <br/> {ReportHtmlLink(repStorGuid)}");
                    _cntPass.Add(item);
                }
                else
                {
                    Log.Error($"Test {testNo,3} Failed {item} ReportId: {reportCriteria.ReportId} - {differences + csvDiffs} Differences Found");
                    Log.Html($"Test {testNo,3} Failed ReportId: {reportCriteria.ReportId} - {differences + csvDiffs} {item.ToDiffLinkHtml()} found between {item.ToHostLinkHtml()} and {item.ToDiffLinkCompareHtml()} {ReportHtmlLink(repStorGuid)}");
                    _cntDiffDocs.Add(item);
                }
                Log.Info($"Test {testNo,3} {ReportHtmlLink(repStorGuid)}");
            }
            catch (Exception exception)
            {
                Log.Warn($"Test {testNo,3} Failed: {item} ReportId: {reportCriteria?.ReportId} {exception.Message}");
                Log.Html($"Test {testNo,3} Failed: {item.ToHostLinkHtml()} <br/> ReportId: {reportCriteria?.ReportId} {exception.Message}");
                _cntFails.Add(item);
            }
            finally
            {
                Log.Timing(_tm);
            }
        }

        public string GetLatestFile(string path, string[] wa, int skip = 0, bool debug = false)
        {
            if (debug) Console.WriteLine($"Path: {path}");
            foreach (var w in wa)
            {
                if (File.Exists($@"{path}\{w}")) return $@"{path}\{w}";
            }
            return string.Empty;
        }
        private string ReportHtmlLink(Guid repStorGuid)
        {
            var html = GetReportHtmlUrl(repStorGuid);
            var pdf = GetReportPdfUrl(repStorGuid);
            var csv = pdf.Substring(0, pdf.Length - 3) + "csv";
            return $"Guid: {repStorGuid} <a href=\"{html}\">Html</a>"
                + " | " + $"<a href=\"{pdf}\">Pdf</a>"
                   + " | " + $"<a href=\"{csv}\">Csv</a>";
        }

        private string TestSummary(int cntTests)
        {
            var list = new List<string> { $" Summary: {Parameter.Instance.AppClient,8} {Parameter.Instance.Test,8}", $"Executed: {cntTests,8} " };
            var passed = cntTests - _cntFails.Count - _cntDiffDocs.Count;
            if (_cntFails.Count > 0 || _cntDiffDocs.Count > 0)
            {
                list.Add($"  Differ: {_cntDiffDocs.Count(),8}");
                if (Parameter.Instance.LogLevel.HasFlag(LogLevel.Warn)) list.AddRange(_cntDiffDocs.Select(x => $"\t\t\t{x}"));
                list.Add($"  Failed: {_cntFails.Count,8} (not counting Different)");
                if (Parameter.Instance.LogLevel.HasFlag(LogLevel.Warn)) list.AddRange(_cntFails.Select(x => $"\t\t\t{x}"));
            }
            list.Add($"  Passed: {passed,8}");
            if (Parameter.Instance.LogLevel.HasFlag(LogLevel.Warn)) list.AddRange(_cntPass.Select(x => $"\t\t\t{x}"));
            //}
            //else list.Add($"All Tests Passed");

            return string.Join(Environment.NewLine, list);
        }

        private string NewFilePath(string _expectedFileName, string ext = "html")
        {
            return Path.ChangeExtension(_expectedFileName, $".{Parameter.Instance.HostName}.{ext}");
        }

        private string CompareToFilePath(string _expectedFileName, string ext = "html")
        {
            return Path.ChangeExtension(_expectedFileName, $".{Parameter.Instance.CompareTo}.{ext}");
        }


        private int CompareHtml(string results, string item, string resultsPath)
        {
            var newFilePath = Path.Combine(resultsPath, $@"{item}.{Parameter.Instance.SaveAs}.html");
            var compPath = Path.Combine(resultsPath, $@"{item}.{Parameter.Instance.CompareTo}.html");
            if (!File.Exists(compPath))
            {
                File.WriteAllText(newFilePath, results);
                Log.Error($"No Html File Comparision for {item} (Previous file {compPath} does not exist) - Saving results only");
                return 0;
            }
            if (File.Exists(newFilePath)) RenamePreviousFile(newFilePath);
            File.WriteAllText(newFilePath, results);
            var expected = File.ReadAllText(compPath);
            if (results == expected) return 0;
            return FindDifferences(item, expected, results, newFilePath);
        }


        private string RenamePreviousFile(string newFilePath, string ext = "html")
        {
            var fi = new FileInfo(newFilePath);
            var renamedFilePath = Path.ChangeExtension(newFilePath, $".prev{fi.CreationTime.ToString("yyyyMMdd.hhmmss")}.{ext}");
            File.Move(newFilePath, renamedFilePath);
            return renamedFilePath;
        }

        private string GetRenamedFileByDate(string newFilePath, string ext = "html")
        {
            //var fi = new FileInfo(newFilePath);
            var renamedFilePath = Path.ChangeExtension(newFilePath, $".{DateTime.Now.ToString("yyyyMMdd.hhmmss")}.{ext}");
            return renamedFilePath;
        }

        private string RemoveKnownHtmlChanges(string results)
        {
            results = results.Replace(@"\r\n", Environment.NewLine).Replace(@"\n", "").Replace(@"\t", string.Format("\t"));
            results = results.Substring(1, results.Length - 2);
            var now = System.DateTime.Now;
            for (int index = 0; index < dtstr.Length; index++)
            {
                var str = now.ToString(dtstr[index]);
                results = Regex.Replace(results, str + dtreg[index], "DATE"); // Remove datetime stamps
            }
            if (Arg.RemoveStyle)
            {
                results = Regex.Replace(results, @"<link.*>", "<!-- style sheet removed -->");
                results = Regex.Replace(results, Arg.Host, ".");
            }
            return results;
        }

        //public async Task DocPackTests()
        //{
        //    Log.Trace();
        //    await CreatePaths();
        //    var docpacJsonPath = Path.Combine(_clientResultsPath, "DocPackGenerationRqCriteria.json");
        //    var docpacPdfPath = Path.Combine(_clientResultsPath, $"{Arg.Test}.Pdf");
        //    var docpacTxtPath = Path.Combine(_clientResultsPath, $"{Arg.Test}.txt");
        //    var docpacTxtPathnew = Path.Combine(_clientResultsPath, $"{Arg.Test}.new.txt");
        //    var pagesPath = Path.Combine(_clientResultsPath, $"pages");
        //    if (!File.Exists(docpacJsonPath)) throw new Exception($"{docpacJsonPath} does not exist");
        //    var testCritJson = File.ReadAllText(docpacJsonPath);
        //    var apiCriteria = JsonConvert.DeserializeObject<DocPackGenerationRq>(testCritJson);
        //    Log.Info($"\nBegin Test {Arg.Test} Results\nmm:ss:ms");
        //    var testPassed = true;
        //    _tm.Reset();
        //    _tm.Start();
        //    Guid criteriaId;
        //    Dictionary<Guid, string> docList = null;
        //    DocPackDistributionSessionModel distributionSessionModel = null;
        //    DocPackGenerationStatus genStatus = null;
        //    var testCount = 0;
        //    try
        //    {
        //        byte[] pdfFileBytes;
        //        if (Arg.ReadFromFile)
        //        {
        //            pdfFileBytes = File.ReadAllBytes(docpacPdfPath);
        //        }
        //        else
        //        {
        //            criteriaId = await _apiRepo.PostAsync<DocPackGenerationRq, Guid>("au/docpack/generation", apiCriteria, "GenerateDocuments");
        //            genStatus = await PollForDocumentReady(criteriaId);
        //            if (genStatus.GenerationStatus == GenerationStatus.Aborted) return;
        //            var clientPartyId = -1; //TODO?
        //            distributionSessionModel = await _apiRepo.CreateDistributionSession(criteriaId, (int)DistributionType.Print, clientPartyId);
        //            docList = await GetCacheDocumentList(criteriaId);
        //            var firstDocumentGuid = distributionSessionModel.DocumentGuids.First();
        //            var fileName = docList[firstDocumentGuid];
        //            fileName = SanitiseFileName(fileName);
        //            pdfFileBytes = await _apiRepo.GetDocPackPdfAsync(criteriaId, firstDocumentGuid);
        //            File.WriteAllBytes(docpacPdfPath, pdfFileBytes);
        //        }
        //        if (Arg.Pages) // This option not in use
        //        {
        //            if (Directory.Exists(pagesPath))
        //            {
        //                await CreatePdfPages(pagesPath, pdfFileBytes, true);
        //                await ComparePages(pagesPath);
        //            }
        //            else
        //            {
        //                Directory.CreateDirectory(pagesPath);
        //                Directory.CreateDirectory(Path.Combine(pagesPath, "ref"));
        //                Directory.CreateDirectory(Path.Combine(pagesPath, "new"));
        //                await CreatePdfPages(pagesPath, pdfFileBytes);
        //            }
        //        }
        //        else
        //        {
        //            if (docList != null && Arg.Html && genStatus != null && genStatus.ReportGeneration.Any()) // Examine each html document in docpack
        //            {
        //                testCount = genStatus.ReportGeneration.Count;
        //                await ProcessDocpackHtmls(apiCriteria, genStatus);
        //            }
        //            else
        //            {// This option doesnt work as the convert to txt uses ocr and there are ocr errors which show up as changes!!!
        //                var txt = await ConvertPdfToText(pdfFileBytes);
        //                var cleanTxt = RemoveKnownDocpackChanges(txt);
        //                if (File.Exists(docpacTxtPath))
        //                {
        //                    File.WriteAllText(docpacTxtPathnew, cleanTxt);
        //                    var expected = File.ReadAllText(docpacTxtPath);
        //                    if (cleanTxt != expected)
        //                    {
        //                        //Compare cleanTxt with expected
        //                        FindDifferences("DocPacK", expected, cleanTxt, docpacTxtPath);
        //                        testPassed = false;
        //                    }
        //                }
        //                else
        //                {
        //                    File.WriteAllText(docpacTxtPath, cleanTxt);
        //                    Log.Info($"{docpacPdfPath} Created");
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        testPassed = false;
        //        Log.Info($"Exception: {exception.Message}");
        //    }
        //    finally
        //    {
        //        if (!testPassed) _cntFails.Add(docpacPdfPath);
        //    }
        //    //_clientResultsPath=Arg.Te
        //    TestSummary(Arg.Test, testCount);
        //}

        //private async Task ProcessDocpackHtmls(DocPackGenerationRq apiCriteria, DocPackGenerationStatus genStatus)
        //{
        //    Log.Trace();
        //    //  foreach (var doc in apiCriteria.DocPackRows)
        //    //{
        //    //    Log.Info($"ReportId:{doc..ReportId} StatusMessage:{doc.StatusMessage} Id:{doc.Id} EntityId:{doc.EntityId}");
        //    //}
        //    var reps = await _apiRepo.GetCachedHtml(genStatus.EntityId);
        //    foreach (var key in reps.Keys)
        //    {
        //        var results = RemoveKnownHtmlChanges(reps[key]);
        //        var expectedFileName = Path.Combine(_htmlResultsPath, $"{key}.html"); //TODO add report name from ReportGeneration status
        //        // Save to file and compare
        //        var testPassed = CompareHtml(expectedFileName, results, key);
        //        if (!testPassed) _cntFails.Add(key);
        //    }

        //}

        private int FindDifferences(string item, string expected, string results, string expectedFileName, bool removeSpaces = false, bool removeQuotes = false, string ext = "")
        {
            if (removeSpaces)
            {
                expected = expected.Replace(" ", "");
                results = results.Replace(" ", "");
            }
            if (removeQuotes)
            {
                expected = expected.Replace("\"", "");
                results = results.Replace("\"", "");
            }
            var diffr = new DiffPlex.DiffBuilder.InlineDiffBuilder(new Differ());
            var dModel = diffr.BuildDiffModel(expected, results);
            var changes = dModel.Lines.Where(t => t.Type != ChangeType.Unchanged).Select(x => $"Line: {x.Position,5} {x.Type,10} {x.Text}").ToList();
            if (changes.Count == 0) return 0;
            Log.Info($"\n{ext} Differences Found for {item}:");
            for (int index = 0; index < Math.Min(_chContext, changes.Count); index++) Log.Info(changes[index]);
            if (changes.Count > _chContext) Log.Info($"{changes.Count - _chContext} more ...");
            File.WriteAllText(Path.ChangeExtension(expectedFileName, $".{ext}diff.txt"), string.Join("\n", changes));
            return changes.Count;
        }

        //private async Task ComparePages(string path)
        //{
        //    var refPageFiles = Directory.GetFiles(Path.Combine(path, "ref"), @"*.txt");
        //    var newPageFiles = Directory.GetFiles(Path.Combine(path, "new"), @"*.txt");
        //    for (int ri = 0; ri < refPageFiles.Length; ri++)
        //    {
        //        var expected = File.ReadAllText(refPageFiles[ri]);
        //        var cleanTxt = File.ReadAllText(newPageFiles[ri]);
        //        FindDifferences("Page", expected, cleanTxt, refPageFiles[ri]);
        //    }
        //}

        private async Task CreatePdfPages(string path, byte[] pdfFileBytes, bool isNew = false)
        {
            var newDir = (isNew ? "new" : "ref");
            var pagesPath = Path.Combine(path, newDir);
            using (var pdfMs = new MemoryStream(pdfFileBytes))
            {
                var doc = new PdfDocument(pdfMs);
                for (int p = 0; p < doc.Pages.Count; p++)
                {
                    var pg = doc.Clone(p, 1);
                    var pgPath = Path.Combine(pagesPath, $"{p}.pdf");
                    pg.Save(pgPath);
                    var pgBytes = File.ReadAllBytes(pgPath);
                    var txt = await ConvertPdfToText(pgBytes);
                    var cleanTxt = RemoveKnownDocpackChanges(txt);
                    File.WriteAllText(Path.Combine(pagesPath, $"{p}.txt"), cleanTxt);
                }
            }
        }

        private string RemoveKnownDocpackChanges(string txt)
        {
            var now = System.DateTime.Now;
            string[] dtstr = { @"d/MM/yyyy ", "dd MMMM yyyy", @"dd/MM/yyyy" };
            string[] dtreg = { @"\d+:\d+:\d+ [APap][mM]", "", "" };
            var t = txt;
            for (int index = 0; index < dtstr.Length; index++)
            {
                var str = now.ToString(dtstr[index]);
                t = Regex.Replace(t, str + dtreg[index], "DATE"); // Remove datetime stamps
            }
            string[] treg = { @"[\u000C]", @"[\ufffd]" };
            string[] trep = { "", "" };
            for (int index = 0; index < treg.Length; index++)
            {
                t = Regex.Replace(t, treg[index], trep[index]); // Remove non print chars
            }

            return t;
        }

        //protected async Task<DocPackDistributionSessionModel> CreateDistributionSession(Guid docPackCriteriaId, int distributionType, int recipientPartyId)
        //{
        //    return await _apiRepo.CreateDistributionSession(docPackCriteriaId, distributionType, recipientPartyId);
        //}

        protected string SanitiseFileName(string fileName)
        {
            return fileName
                .Replace("\\", "")
                .Replace("/", "")
                .Replace(":", "")
                .Replace("*", "")
                .Replace("?", "")
                .Replace("\"", "")
                .Replace("<", "")
                .Replace(">", "")
                .Replace("|", "")
                .Replace("\"", "")
                .Replace("\t", "");
        }

        private string GetReportHtmlUrl(Guid repStorGuid) => $"{Arg.Host}/{Appclient}/reports/generate/{repStorGuid}/view/html";
        private string GetReportPdfUrl(Guid repStorGuid) => $"{Arg.Host}/{Appclient}/reports/generate/{repStorGuid}/view/pdf";
        private async Task<string> GetReportHtml(Guid repStorGuid)
        {
            var json = await _apiRepo.GetJson(GetReportHtmlUrl(repStorGuid));
            return json;
        }

        private async Task GetSaveReportPdf(Guid repStorGuid, string item)
        {
            var pdf = await GetReportPdf(repStorGuid);
            var saveas = string.IsNullOrWhiteSpace(Parameter.Instance.SaveAs) ? Parameter.Instance.HostName : Parameter.Instance.SaveAs;
            var pdfPath = Path.Combine(_htmlResultsPath, $@"{item}\{item}.{saveas}.pdf");
            File.WriteAllBytes(pdfPath, pdf);
        }

        private async Task<byte[]> GetReportPdf(Guid repStorGuid)
        {
            var getUlr = $"{Arg.Host}/{Appclient}/reports/generate/{repStorGuid}/view/pdf";
            var pdf = await ApiRepo.GetAsByteArrayAsync(getUlr, "GetReportPdf");
            return pdf;
        }

        private async Task WaitForReportGeneration(Guid repStorGuid)
        {
            var startTime = DateTime.Now;
            var getUlr = $"{Arg.Host}/{Appclient}/reports/generate/{repStorGuid}/status";
            Log.Trace(getUlr);
            var maxWaitForDocumentReady = Arg.Timeout;
            var sleepTime = 2000;
            var interations = 1000*maxWaitForDocumentReady/sleepTime;
            for (int i = 0; i < interations; i++)
            {
                var elapsedTime = DateTime.Now.Subtract(startTime).TotalMilliseconds;
                var json = await _apiRepo.GetJson(getUlr);
                var status = JsonConvert.DeserializeObject<ReportGenerationStatus>(json);
                var lastException = status.LastException;//status["LastException"].ToString();
                if (!string.IsNullOrEmpty(lastException?.Message))
                {
                    var exceptionMessage = status.LastException?.ExceptionMsg;//["ExceptionMessage"].ToString();
                    throw new Exception(string.IsNullOrEmpty(exceptionMessage) ? lastException.Message : exceptionMessage);
                }
                var docCreationProgress = (DocCreationProgress)status.DocCreationProgress;
                ShowStatus(docCreationProgress, elapsedTime);
                if (status.IsCompletedState) return;
                System.Threading.Thread.Sleep(sleepTime);
            }
            throw new Exception($"Max Queue Wait Time ({maxWaitForDocumentReady}s) Exceeded");
        }

        private Dictionary<DocCreationProgress, string> _docCreationProgressDictionary = new Dictionary<DocCreationProgress, string>()
                                                                                         {
                                                                                             {DocCreationProgress.New, "Initializing" },
                                                                                             {DocCreationProgress.Started, "Initialized" },
                                                                                             {DocCreationProgress.FetchingModel, "Getting Data" },
                                                                                             {DocCreationProgress.ModelFetched, "Data Retrieved" },
                                                                                             {DocCreationProgress.RenderingCsv, "Rendering Csv" },
                                                                                             {DocCreationProgress.CsvRendered, "Csv Rendered" },
                                                                                             {DocCreationProgress.RenderingHtml, "Rendering Html" },
                                                                                             {DocCreationProgress.HtmlRendered, "Html Rendered" },
                                                                                             {DocCreationProgress.RenderingPdf, "Rendering PDF" },
                                                                                             {DocCreationProgress.PdfRendered, "Pdf Rendered" },
                                                                                             {DocCreationProgress.Completed, "Generation Complete" },
                                                                                         };
        private DocCreationProgress _lastDocCreationProgress = DocCreationProgress.New;
        private double _progessStageStart = 0;
        private const int MillsecsPerMinute = 60000;
        private const int PdfRenderFactor = 5;
        protected void ShowStatus(DocCreationProgress docCreationProgress, double elapsedTime)
        {
            if (_lastDocCreationProgress != docCreationProgress)
            {
                var msg = _docCreationProgressDictionary[_lastDocCreationProgress];
                if (_lastDocCreationProgress != DocCreationProgress.New)
                    Log.Info($"{msg} - Complete {(elapsedTime - _progessStageStart) / MillsecsPerMinute:N2} mins");
                msg = _docCreationProgressDictionary[docCreationProgress];
                _progessStageStart = elapsedTime;
                if (docCreationProgress == DocCreationProgress.RenderingPdf) msg += $" - Estimated {(_progessStageStart * PdfRenderFactor) / MillsecsPerMinute:N2} mins";
                Log.Info(msg);
                _lastDocCreationProgress = docCreationProgress;
            }
        }

        private async Task<string> GetReportCriteria(string report)
        {
            //GET 
            var getUlr = $"{Arg.Host}/{Appclient}/reports/testing/criteria/{report}/0";
            Log.Trace(getUlr);
            var json = await _apiRepo.GetJson(getUlr);
            return json;
        }

        private async Task<Guid> QueueReport(ReportCriteria reportCriteria)
        {
            //POST 
            int flags = 0;
            if (Parameter.Instance.Html) flags |= 1;
            if (Parameter.Instance.Pdf) flags |= 2;
            if (Parameter.Instance.Csv) flags |= 4;
            var postUrl = $"{Arg.Host}/{Appclient}/reports/generate/queue/{flags}";
            Log.Trace(postUrl);
            //var resp = await PostJson(postUrl, json);
            var guid = await _apiRepo.PostAsync<ReportCriteria, Guid>(postUrl, reportCriteria, "QueueReport");
            // var guid = JsonConvert.DeserializeObject<Guid>(resp);
            return guid;
        }

        protected async Task<List<string>> GetDocPackPdfsAsync(Guid criteriaId, IEnumerable<Guid> documentGuids, string documentsTempFilePath)
        {
            var docList = await GetCacheDocumentList(criteriaId);
            var filePaths = new List<string>();
            foreach (var documentGuid in documentGuids)
            {
                var fileName = docList[documentGuid];
                //   fileName = SanitiseFileName(fileName);
                // filePaths.Add(await GetDocPackPdfAsync(criteriaId, documentGuid, fileName, documentsTempFilePath));
            }
            return filePaths;
        }

        protected async Task<Dictionary<Guid, string>> GetCacheDocumentList(Guid criteriaId)
        {
            // get a dictionary of doc Guid -> file name so we know how to name the files
            return await _apiRepo.GetCacheDocumentList(criteriaId);
        }

        //protected async Task<DocPackGenerationStatus> PollForDocumentReady(Guid docPackGuid)//, DistributionListViewModelBase.ShowProgressMessageDelegate showProgressMessageMethod)
        //{
        //    Log.Trace();
        //    var startTime = DateTime.Now;

        //    // 1. GET Poll /generation/status/{docPackGuid} until TotalReportCount > 0    (this indicates number of reports prepared for generation)
        //    // 2. GET Get the sub report list /generation/reports/{docPackGuid} to get actual generation notification list for each report (to show grid on screen)
        //    // 3. GET poll reports generation/status/{docPackGuid}/reports
        //    //              OR     generation/status/{docPackGuid}/reports/reportIdArr 
        //    //              to get generation status list for reports in order to update on-screen grid (by guid id)
        //    //          keep polling until every item on-screen has reached a completed state (ie ready, or error etc)...
        //    // 4. GET Poll /generation/status/{docPackGuid} until ready or error etc
        //    var status = await _apiRepo.GetDocPackStatus(docPackGuid);
        //    var docPackStatus = status.GenerationStatus;
        //    var totalReportCount = status.TotalReportCount;
        //    while (totalReportCount == 0 && docPackStatus != GenerationStatus.Error && docPackStatus != GenerationStatus.Aborted)
        //    {
        //        await Task.Delay(ApiRepo.Settings.DocumentStatusPollingIntervalMS);

        //        var elapsedTime = DateTime.Now.Subtract(startTime).TotalMilliseconds;
        //        if (elapsedTime > ApiRepo.Settings.MaxDocumentStatusPollingPeriodMinutes * 60000)
        //        {
        //            Log.Error(string.Format("The maximum generation time of {0} minutes has been exceeded and generation of the document pack has been aborted. Please try again.",
        //                ApiRepo.Settings.MaxDocumentStatusPollingPeriodMinutes));
        //            break;
        //        }

        //        status = await _apiRepo.GetDocPackStatus(docPackGuid);
        //        docPackStatus = status.GenerationStatus;
        //        totalReportCount = status.TotalReportCount;
        //        Log.Progress($"{totalReportCount,3} documents      ");
        //    }
        //    Log.Status(status);

        //    IEnumerable<DocPackGenerationNotification> reportDocs = await _apiRepo.GetDocPackReportDocs(docPackGuid);
        //    int remainingDocCount = totalReportCount;
        //    var stalledOnFailedDocument = false;
        //    var isSecondTry = false;
        //    while (remainingDocCount > 0 && !stalledOnFailedDocument)
        //    {
        //        if (isSecondTry)
        //        {
        //            await Task.Delay(ApiRepo.Settings.DocumentStatusPollingIntervalMS);
        //        }
        //        IEnumerable<IReportGenerationStatus> reportItemsStatuses = await _apiRepo.GetDocPackReportItemsStatuses(docPackGuid);
        //        remainingDocCount = totalReportCount;
        //        var documentProgressId = 600000000; // just a seed to get docs 
        //        foreach (var reportItemsStatus in reportItemsStatuses)
        //        {
        //            documentProgressId++;
        //            if (reportItemsStatus.GenerationStatus == GenerationStatus.Ready ||
        //                    reportItemsStatus.GenerationStatus == GenerationStatus.Error ||
        //                    reportItemsStatus.GenerationStatus == GenerationStatus.Aborted)
        //            {
        //                remainingDocCount--;
        //                Log.Status(reportItemsStatus);
        //            }
        //            Log.Progress($"{remainingDocCount,3} documents remaining");
        //            stalledOnFailedDocument = StalledOnFailedDocument(reportItemsStatuses);
        //        }

        //        var elapsedTime = DateTime.Now.Subtract(startTime).TotalMilliseconds;
        //        if (elapsedTime > ApiRepo.Settings.MaxDocumentStatusPollingPeriodMinutes * 60000)
        //        {
        //            Log.Error($"The maximum generation time of {ApiRepo.Settings.MaxDocumentStatusPollingPeriodMinutes} minutes has been exceeded and generation of the document pack has been aborted. Please try again.");
        //            return status;
        //        }
        //        isSecondTry = true;
        //    }

        //    var generationStatus = await _apiRepo.GetDocPackStatus(docPackGuid);
        //    docPackStatus = generationStatus.GenerationStatus;
        //    Log.Status(generationStatus);
        //    while (docPackStatus != GenerationStatus.Error && docPackStatus != GenerationStatus.Aborted && docPackStatus != GenerationStatus.Ready)
        //    {
        //        await Task.Delay(ApiRepo.Settings.DocumentStatusPollingIntervalMS);

        //        var elapsedTime = DateTime.Now.Subtract(startTime).TotalMilliseconds;
        //        if (elapsedTime > ApiRepo.Settings.MaxDocumentStatusPollingPeriodMinutes * 60000)
        //        {
        //            Log.Error($"The maximum generation time of {ApiRepo.Settings.MaxDocumentStatusPollingPeriodMinutes} minutes has been exceeded and generation of the document pack has been aborted. Please try again.");
        //            break;
        //        }

        //        generationStatus = await _apiRepo.GetDocPackStatus(docPackGuid);
        //        docPackStatus = generationStatus.GenerationStatus;
        //    }
        //    return generationStatus;
        //}

        //private bool StalledOnFailedDocument(IEnumerable<IReportGenerationStatus> reportItemsStatuses)
        //{
        //    if (reportItemsStatuses == null) return false;
        //    //   if (!reportItemsStatuses.Any(ri => ri..ReportId == (int)ReportID.TableOfContents)) return false;

        //    var anyErrors = reportItemsStatuses.Any(ri => ri.GenerationStatus == GenerationStatus.Error);
        //    var numberNonTocDocs = reportItemsStatuses.Count();
        //    var numberNonTocDocsCompleted =
        //        reportItemsStatuses.Count(
        //            ri =>
        //                (ri.GenerationStatus == GenerationStatus.Ready || ri.GenerationStatus == GenerationStatus.Error ||
        //                 ri.GenerationStatus == GenerationStatus.Aborted));

        //    if (anyErrors && numberNonTocDocs == numberNonTocDocsCompleted) return true;
        //    return false;
        //}

        public async Task<string> ConvertPdfToText(byte[] pdfBytes)
        {
            var extr = new PdfExtract.Extractor();
            var s = extr.ExtractText(new MemoryStream(pdfBytes));
            string txt = "";
            using (TextReader tr = new StreamReader(s))
            {
                txt = tr.ReadToEnd();
            }
            return txt;
        }

        public void Validate()
        {
            Parameter.Instance.Validate();
        }


        private static string BytesToStringConverted(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        #region Csv Handling

        private async Task GetSaveReportCsv(Guid repStorGuid, int reportCriteriaReportId, string item)
        {
            var csv = await GetReportCsv(repStorGuid);
            SaveReportCsv(item, csv);
        }

        private void SaveReportCsv(string item, string csv)
        {
            var saveas = string.IsNullOrWhiteSpace(Parameter.Instance.SaveAs) ? Parameter.Instance.HostName : Parameter.Instance.SaveAs;
            var path = Path.Combine(_htmlResultsPath, $@"{item}\{item}.{saveas}.csv");
            File.WriteAllText(path, csv);
        }

        private async Task<string> GetReportCsv(Guid repStorGuid)
        {
            var getUlr = $"{Arg.Host}/{Appclient}/reports/generate/{repStorGuid}/view/csv";
            var csvBytes = await ApiRepo.GetAsByteArrayAsync(getUlr, "GetSaveReport");
            var csvStr = BytesToStringConverted(csvBytes);
            return csvStr;
        }


        private int CompareCsv(string csv, string item)
        {
            var ext = "csv";
            var saveas = string.IsNullOrWhiteSpace(Parameter.Instance.SaveAs) ? Parameter.Instance.HostName : Parameter.Instance.SaveAs;
            var filePath = Path.Combine(_htmlResultsPath, $@"{item}\{item}.{saveas}.{ext}");
            var newFilePath = filePath;
            var compareToFilePath = Path.Combine(_htmlResultsPath, $@"{item}\{item}.{Parameter.Instance.CompareTo}.{ext}");
            //if (File.Exists(newFilePath))
            //{
            //    newFilePath = GetRenamedFileByDate(newFilePath, ext);
            //}
            if (!File.Exists(compareToFilePath))
            {
                Log.Info($"No CSV File Comparision for {item} (Previous file {compareToFilePath} does not exist) - Saving results only");
                File.WriteAllText(filePath, csv);
                return 0;
            }
            var expected = File.ReadAllText(compareToFilePath);
            if (csv == expected)
            {
                return 0;
            }
            RenamePreviousFile(compareToFilePath, ext);
            File.WriteAllText(newFilePath, csv);
            return FindDifferences(item, expected, csv, newFilePath, false, true, ext);
        }

        #endregion Csv

    }

}
