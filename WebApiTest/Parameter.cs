﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WebApiTest
{
    using System;
    using System.IO;
    using System.Linq;

    public static class Parameter
    {
        private static Param _instance;
        public static Param Instance => _instance;
        internal static LogLevel DefaultLogLevel => LogLevel.Info | LogLevel.Summary | LogLevel.Warn | LogLevel.Error | LogLevel.Fatal;
        public static string HostDefault => "local";

        public static void Create(Param param)
        {
            _instance = param;
        }

        public static void Create(string jsonPath)
        {
            var json = File.ReadAllText(jsonPath);
            _instance = JsonConvert.DeserializeObject<Param>(json);
        }

        public static IEnumerable<string> InfoList()
        {
            var properties = from p in typeof(Param).GetProperties() where p.CanWrite select p;
            return  properties.Where(x => x.GetValue(_instance) != null && x.GetValue(_instance).ToString() != "")
                .Select(z=>z).ToList()
                .Select(y => $"--{y.Name} {GetParameterValue(y)}");
        }

        private static object GetParameterValue(PropertyInfo info)
        {
            return (info.Name == "Pwd") ? "{hashed password}" : info.GetValue(_instance);
        }

        public static string Info()
        {
            return "\n"+String.Join("\n", InfoList());
        }
        
    }
}
