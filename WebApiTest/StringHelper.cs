﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTest
{
    public static class StringHelper
    {
        public static string ToHostLinkHtml(this string item)
        {
            return $"<a href='{item}.{Parameter.Instance.HostName}.html'>{item}.{Parameter.Instance.HostName}</a> <a href='{item}.{Parameter.Instance.HostName}.pdf'>(pdf)</a>";
        }
               
        public static string ToDiffLinkHtml(this string item)
        {
            return $"<a href='{item}.{Parameter.Instance.HostName}.diff.txt'>differences</a>";
        }

        public static string ToDiffLinkCompareHtml(this string item)
        {
            return $"<a href='{item}.{Parameter.Instance.CompareTo}.html'>{item}.{Parameter.Instance.CompareTo}</a> <a href='{item}.{Parameter.Instance.CompareTo}.pdf'>(pdf)</a>";
        }

        //public static Uri ToRelativePath(this string path, string toPath)
        //{
        //    var uri = new Uri(path);
        //    return uri.MakeRelativeUri(new Uri(toPath));
        //}

        //public static Uri ToUri(this string path)
        //{
        //    return new Uri(new Uri(ThisIpAddress), path);
        //}

        //private static string ThisIpAddress => Dns.GetHostEntry(Dns.GetHostName()).AddressList.First(f =>f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
    }
}
