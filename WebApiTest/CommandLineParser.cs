﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiffPlex;
using Fclp;

namespace WebApiTest
{
    public static class CommandLineParser
    {
        public static ICommandLineParserResult ParseCommandLine(string[] args)
        {
            // Parse Command Line:
            var p = new FluentCommandLineParser<Param> {IsCaseSensitive = false};

            p.Setup(a => a.AppClient).As('a', "AppClient").SetDefault(AppClientEnum.None).WithDescription("AU or NZ (from current path if not specified)");
            p.Setup(a => a.GetCriteria).As('c', "GetCriteria").WithDescription("Retrive Criteria from api");
            p.Setup(a => a.Test).As('d', "Test").WithDescription("Use DocPack list under the [AU/NZ] subfolder");
            p.Setup(a => a.Results).As('r', "Results").WithDescription("folder path for Test Results");

            p.Setup(a => a.Html).As("Html").SetDefault(true).WithDescription("Do Html tests (default)");
            p.Setup(a => a.Pdf).As("Pdf").WithDescription("Create pdfs");
            p.Setup(a => a.Csv).As("Csv").WithDescription("Create Csvs");
            p.Setup(a => a.Generate).As("Generate").SetDefault(false).WithDescription("if true Call web api to generate html (if false only if no previous output for this host)");

            p.Setup(a => a.CreateLogFile).As("createlogfile").SetDefault(false).WithDescription("Create a daily named log file default is in current dir");
            p.Setup(a => a.SlackOnFail).As("slackonfail").SetDefault(false).WithDescription("Send Slack Notification on any failure");
            p.Setup(a => a.LogDir).As("logdir").SetDefault("").WithDescription("alternative folder path for logs");
            p.Setup(a => a.LogTargetInt).As("logtarget").SetDefault((int)LogTarget.Console).WithDescription("Where to send log info Console, Debug, File");
            p.Setup(a => a.LogLevelInt).As("LogLevel").SetDefault((int)Parameter.DefaultLogLevel).WithDescription("LogLevel Flags eg Warn Error Fatal"); ;
            
            p.Setup(a => a.ParFile).As("ParFile").WithDescription("Read parameters from .json file");
            p.Setup(a => a.File).As("File").WithDescription("a File to Test with");
            p.Setup(a => a.Pages).As("Pages").WithDescription("exract pages of pdf");

            p.Setup(a => a.Skip).As('s', "Skip").WithDescription("runs to skip").SetDefault(0);
            p.Setup(a => a.Take).As('t', "Take").WithDescription("Count of runs before exit").SetDefault(500);

            p.Setup(a => a.HostName).As("Host").WithDescription($"Host default {Parameter.HostDefault}").SetDefault(Parameter.HostDefault);
            p.Setup(a => a.CompareTo).As("CompareTo").WithDescription(" Host name to compare with (default is Same as Host");
            p.Setup(a => a.SaveAs).As("SaveAs").WithDescription("Save generated files as xxx.this.yyy");
            p.Setup(a => a.User).As("User").WithDescription("User email").SetDefault(ConfigurationManager.AppSettings["User"]);
            p.Setup(a => a.Pwd).As("Pwd").WithDescription("User Password").SetDefault(ConfigurationManager.AppSettings["Pwd"]);
            p.Setup(a => a.Timeout).As("timeout").WithDescription("Time (sec) to wait for api call return").SetDefault(60);
            p.Setup(a => a.RemoveStyle).As("RemoveStyle").WithDescription("Remove style sheet reference from html").SetDefault(true);
            p.SetupHelp("?", "h", "help").UseForEmptyArgs().Callback(h => Log.Info($"WebApiTest Help:\n{h}\n\n "));

            var result = p.Parse(args);
            if (string.IsNullOrWhiteSpace(p.Object.ParFile)) Parameter.Create(p.Object);
            else Parameter.Create(p.Object.ParFile);

            Parameter.Instance.LogLevel = (LogLevel)Parameter.Instance.LogLevelInt;
            Parameter.Instance.LogTarget = (LogTarget)Parameter.Instance.LogTargetInt;
            if (string.IsNullOrEmpty(Parameter.Instance.CompareTo)) Parameter.Instance.CompareTo = Parameter.Instance.HostName;
            if (Parameter.Instance.CreateLogFile|| !string.IsNullOrEmpty(Parameter.Instance.LogDir)) Parameter.Instance.LogTarget |= LogTarget.File;
            if (Parameter.Instance.AppClient == AppClientEnum.None && Parameter.Instance.Test!=null) Parameter.Instance.AppClient = Environment.CurrentDirectory.Contains("AU") || Parameter.Instance.Test.Contains("AU") ? AppClientEnum.AU : AppClientEnum.NZ;
            if (result.HasErrors && !string.IsNullOrWhiteSpace(result.ErrorText)) Log.Error(result.ErrorText);
            Parameter.Instance.HelpCalled = result.HelpCalled;
            if (!result.HelpCalled && result.EmptyArgs)p.HelpOption.ShowHelp(p.Options);
            return result;
        }
    }
}
