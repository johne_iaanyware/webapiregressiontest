﻿using System;

namespace WebApiTest
{
    [Flags]
    public enum LogLevel
    {
        None = 0x0,
        Debug = 0x1,
        Error = 0x2,
        Fatal = 0x4,
        Info = 0x8,
        Trace = 0x10,
        Warn = 0x20,
        Timing = 0x40,
        Summary = 0x80,
        All = Summary| Debug | Error | Fatal | Info | Trace | Warn,
    }
}