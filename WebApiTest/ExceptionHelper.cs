using System;

namespace WebApiTest
{
    public class ExceptionHelper
    {
        public static string GetExceptionMessage(Exception ex)
        {
            string Message = null;
            Message = ex.Message;
            if (ex.InnerException != null)
            {
                Message += Environment.NewLine + "Exception Detail: " + ex.InnerException.Message;
                if (ex.InnerException.InnerException != null)
                {
                    Message += Environment.NewLine + "Further Detail: " + ex.InnerException.InnerException.Message;
                }
            }
            return Message;
        }
    }
}