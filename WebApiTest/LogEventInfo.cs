using System;
using System.Web.Http;

namespace WebApiTest
{
    public class LogEventInfo
    {
        private LogTarget _name;
        private string _message;
        private DateTime _dt;

        public LogEventInfo(LogLevel info, string message)
        {
            Level = info;
            _message = message;
            _dt = DateTime.Now;
        }

        public LogLevel Level { get; }

        public string ToString(bool includeDate = false)
        {
            var tm = includeDate ? _dt.ToString("T") + " " : "";
            var lev = Level == LogLevel.Info ? "          " : $"{Level,8}: ";
            return $"{tm}{lev}{_message}";
        }
    }
}