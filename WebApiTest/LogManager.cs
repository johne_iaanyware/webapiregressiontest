using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace WebApiTest
{
    [Flags]
    public enum LogTarget
    {
        None = 0x0,
        Console = 0x1,
        Debug = 0x2,
        File = 0x4,
        Html = 0x8
    }

    public class LogManager
    {
        public static List<LogEventInfo> Messages = new List<LogEventInfo>();
        public static List<LogEventInfo> HtmlMessages = new List<LogEventInfo>();

        private readonly string _logFileName = $"WebApiTest_{DateTime.Now.Date.ToString("yyyy-MM-dd")}.log";
        public static readonly string _htmlLogFileName = $"WebApiTest_{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")}.html";
        private string _logFile = "";
        private string _htmlLog = "";

        public LogTarget Name { get; set; } = LogTarget.Console | LogTarget.Debug;

        public void Log(LogEventInfo lInfo, LogTarget logTarget = LogTarget.Console | LogTarget.Debug)
        {
            //TODO add comand line arg for changing default LOgTarget
            if (logTarget.HasFlag(LogTarget.Debug)) Debug.WriteLine(lInfo.ToString());
            if (Parameter.Instance != null && !((LogLevel)Parameter.Instance.LogLevel).HasFlag(lInfo.Level)) return;
            if (logTarget.HasFlag(LogTarget.Console)) Console.WriteLine(lInfo.ToString());
        }

        //logfile target which is appended named with current date
        public void DailyLogAppendMessage()
        {
            try
            {
                if (Parameter.Instance.LogTarget == LogTarget.None) return;
                if (Parameter.Instance.LogTarget.HasFlag(LogTarget.File) && !String.IsNullOrEmpty(Parameter.Instance.LogDir))
                {
                    if (!Directory.Exists(Parameter.Instance.LogDir))
                        Directory.CreateDirectory(Parameter.Instance.LogDir);
                    if (String.IsNullOrEmpty(_logFile)) _logFile = Path.Combine(Parameter.Instance.LogDir, _logFileName);
                    File.AppendAllText(_logFile, GetTextMessages() + Environment.NewLine);
                }
                if (Parameter.Instance.LogTarget.HasFlag(LogTarget.Html))
                {
                    var logpth = (String.IsNullOrEmpty(Parameter.Instance.LogDir)) ? Path.Combine(Parameter.Instance.Test, "html") : Parameter.Instance.LogDir;
                    if (String.IsNullOrEmpty(_htmlLog)) _htmlLog = Path.Combine(logpth, _htmlLogFileName);
                    File.WriteAllText(_htmlLog, GetHtmlMessages() + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"LogDir: {_logFile}\nException: {ex.Message}");
            }
        }

        private string GetTextMessages()
        {
            var mtx = Messages.Select(x => x.ToString(false));
            return String.Join(Environment.NewLine, mtx) + "\n\n";
        }

        private string GetHtmlMessages()
        {
            var mtx = HtmlMessages.Select(x => x.ToString(false));
            return "<html><body>" + String.Join(" ", mtx) + "</body></html>";
            //return "<html><body>" + String.Join("<br/>", mtx) + "<br/></body></html>";
        }
    }
}