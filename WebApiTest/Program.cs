﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTest
{
    //DONE  1. output to be slacked 
    //TODO  2. run remotely? listen for ??
    //TODO  3. Capture docpack json from iAdviser (by iA Staff member only) or Robomongo?
    //DONE  4. Deployment to users local machine
    //TODO  5. Take list (test batch) of docpacks and run in sequence 
    class Program
    {
        private static Param Arg => Parameter.Instance;

        static int Main(string[] args)
        {
          //  Console.WriteLine(string.Join(" ", args));
            var t = Task.Run(() => MainAsync(args));
            t.Wait();
            var passed = t.Result;
            return (passed) ? 0 : 1;
        }

        static async Task<bool> MainAsync(string[] args)
        {
            CommandLineParser.ParseCommandLine(args);
            Log.Html("WebApiRegressionTest "+string.Join(" ",Parameter.InfoList()),LogLevel.Info);
            Tester tester = new Tester(Arg.AppClient);
            var passed = false;
            try
            {
                tester.Validate();
                if (Arg.Html)
                {
                    passed = await tester.HtmlTests();
                }
                //else if (!string.IsNullOrEmpty(Arg.Test))
                //{
                //    await tester.DocPackTests();
                //} else throw new Exception("Test and/or Html Option must be supplied");
            }
            catch (Exception exception)
            {
                Log.Fatal($"Failed!!! \n{exception.Message} ");
                return false;
            }
            finally
            {
                Log.DailyLogAppend();
                if (!passed && Parameter.Instance.SlackOnFail)
                {
                    SendSlackNotification(tester.TestName, tester.TestName + ".Log", Log.AllMessagesString);
                }
            }
            return passed;
        }

        private static void SendSlackNotification(string title, string fileName, string message)
        {
            var urlWithAccessToken = ConfigurationManager.AppSettings["UrlWithAccessToken"];
            var apiToken = ConfigurationManager.AppSettings["ApiToken"];
            //var sc = new SlackClient(urlWithAccessToken);
            var sh = new SlackHelper();
            sh.SlackSendFile(title, fileName, message.ToString(), "%23notifications", apiToken);
        }

    }
}
