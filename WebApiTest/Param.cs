﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;

namespace WebApiTest
{
    public class Param
    {
        private string _hostName;
        public bool Html { get; internal set; }
        public bool Pdf { get; internal set; }
        public AppClientEnum AppClient { get; set; }
        public bool GetCriteria { get; set; }
        public int Skip { get; internal set; }
        public int Take { get; internal set; }

        public string CompareTo { get; set; }

        public string HostName
        {
            get { return _hostName?.ToLower(); }
            set { _hostName = value; }
        }

        public string Host => ConfigurationManager.AppSettings.Get(HostName);
        public string User { get; set; }
        public string Pwd { get; set; }
        public string Test { get; set; }
        public string TestFolder => Path.GetFileName(Test);
        public string ParFile { get; set; }
        public string File { get; set; }
        public bool Pages { get; set; }
        public string Results { get; set; }
        public string LogDir { get; set; }

        internal LogLevel LogLevel { get; set; }
        internal int LogLevelInt { get; set; }

        public LogTarget LogTarget { get; set; } = LogTarget.Console | LogTarget.Debug;
        internal int LogTargetInt { get; set; }
        public bool CreateLogFile { get; set; }
        public bool SlackOnFail { get; set; }
        public bool Generate { get; set; }
        public bool HelpCalled { get; set; } = false;
        public bool Csv { get; set; }
        public int Timeout { get; set; }
        public bool RemoveStyle { get; set; }
        public string SaveAs { get; set; }

        public void Validate()
        {
            if (HelpCalled) return;
            if (!Html) throw new Exception($"Only Html tests currently supported");
            if (string.IsNullOrEmpty(Host)) throw new Exception($"Invalid Host: {HostName}");
        }
    }
}