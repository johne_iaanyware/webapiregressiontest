﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Fclp;
using Ia.Reports.Models.Interfaces;

namespace WebApiTest
{
    public static class Log
    {
        private static readonly LogManager L = new LogManager();//.GetCurrentClassLogger();

        public static void Info(string message)
        {
            MakeLog(LogLevel.Info, message);
        }

        public static void Error(string message)
        {
            MakeLog(LogLevel.Error, message);
        }

        public static void Warn(string message)
        {
            MakeLog(LogLevel.Warn, message);
        }

        public static void Fatal(string message)
        {
            MakeLog(LogLevel.Fatal, message);
        }

        public static void Debug(string message)
        {
            MakeLog(LogLevel.Debug, message);
        }

        public static bool ContainsFatal()
        {
            return LogManager.Messages.Any(x => x.Level >= LogLevel.Fatal);
        }

        public static bool Contains(LogLevel level)
        {
            return LogManager.Messages.Any(x => x.Level >= level);
        }

        public static int ErrorCount(LogLevel level)
        {
            return LogManager.Messages.Count(x => x.Level >= level);
        }

        public static void Trace(string msg = "", [CallerMemberName] string caller = "")
        {
            var message = $"{caller,-20}: {msg}";
            var lInfo = new LogEventInfo(LogLevel.Trace, message);
            LogManager.Messages.Add(lInfo);
            L.Log(lInfo);
        }

        public static void Progress(string message = "")
        {
            Console.Write(message + "  \r");
        }

        public static void Exception(Exception exception)
        {
            var msg = ExceptionHelper.GetExceptionMessage(exception);
            Log.Fatal($"Exception: {msg}");
        }

        public static void Error(ICommandLineParserResult result)
        {
            var messages = result.ErrorText.Split('\n');
            foreach (var error in messages)
            {
                if (!string.IsNullOrEmpty(error)) Error(error);
            }
        }

        public static IEnumerable<LogEventInfo> AllMessages => LogManager.Messages.Where(x => x.Level == LogLevel.Info || x.Level == LogLevel.Warn || x.Level == LogLevel.Error || x.Level == LogLevel.Fatal);
        public static string AllMessagesString => string.Join(Environment.NewLine, AllMessages.Select(x => x.ToString(true))) + "\n\n";

        public static void DailyLogAppend()
        {
            if (Parameter.Instance.LogTarget.HasFlag(LogTarget.File) || Parameter.Instance.LogTarget.HasFlag(LogTarget.Html))
                L.DailyLogAppendMessage();
        }

        //public static void Status(IReportGenerationStatus reportItemsStatus)
        //{
        //    if (reportItemsStatus.GenerationStatus == GenerationStatus.Error || reportItemsStatus.GenerationStatus == GenerationStatus.Aborted)
        //        Log.Error($"report {reportItemsStatus.ReportId} {reportItemsStatus.GenerationStatus}, {reportItemsStatus.StatusMessage} {reportItemsStatus.LastException.Message}");

        //}

        public static void Timing(Stopwatch tm)
        {
            MakeLog(LogLevel.Timing, $"{tm.Elapsed:mm\\:ss\\:ff}");
        }

        private static void MakeLog(LogLevel level,string message)
        {
            var lInfo = new LogEventInfo(level, message);
            LogManager.Messages.Add(lInfo);
            L.Log(lInfo);
        }

        public static void Html(string message, LogLevel lev=LogLevel.None)
        {
            if (lev != LogLevel.None) MakeLog(lev, message); ;
            var html = (lev == LogLevel.None) ? $"{message}<br/>" : $"<pre>{message}</pre>";
            var lInfo = new LogEventInfo(LogLevel.Info, html);
            LogManager.HtmlMessages.Add(lInfo);
        }
    }
}
