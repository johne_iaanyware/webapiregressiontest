﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace WebApiTest
{
    public class SlackHelper
    {

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public void SlackSendFile(string title, string fileName, string content, string channel, string apiToken)
        {
            using (Stream str = GenerateStreamFromString(content))
            {
                byte[] fBytes = new byte[str.Length];
                str.Read(fBytes, 0, fBytes.Length);
                str.Close();

                var webClient = new WebClient();
                string boundary = "------------------------" + DateTime.Now.Ticks.ToString("x");
                webClient.Headers.Add("Content-Type", "multipart/form-data; boundary=" + boundary);
                var fileData = webClient.Encoding.GetString(fBytes);
                var package = string.Format($"--{boundary}\r\nContent-Disposition: form-data; name=\"file\"; filename=\"{fileName}\"\r\nContent-Type: multipart/form-data\r\n\r\n{fileData}\r\n--{boundary}--\r\n");
                //var package = string.Format($"file={content}");

                var nfile = webClient.Encoding.GetBytes(package);
                //byte[] postArray = Encoding.ASCII.GetBytes(package);
                string url = $"https://slack.com/api/files.upload?token={apiToken}&filename={fileName}&title={title}&channels={channel}&pretty=1";
                //string url = $"https://slack.com/api/files.upload?token={apiToken}&content=a%20test&filename=x.txt&channels={channel}&pretty=1";
                // var url = "https://slack.com/api/files.upload?token=xoxp-4788153793-4857537191-78990071574-a638a3e18f&content=another%20is%20a%20test&filename=test_file.txt&title=a%20Test&channels=%23notifications&pretty=1";
                Debug.WriteLine(url);
                //webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] resp = webClient.UploadData(url, "POST", nfile);

                var k = System.Text.Encoding.Default.GetString(resp);

                Debug.WriteLine(k);
            }
        }

    }
}
