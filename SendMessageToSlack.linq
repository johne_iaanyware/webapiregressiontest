<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\SMDiagnostics.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Net.Http.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.ServiceModel.Internals.dll</Reference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>System.Collections.Specialized</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>System.Net.Http</Namespace>
  <Namespace>System.Net.Http.Headers</Namespace>
  <Namespace>System.Runtime.Serialization.Json</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
</Query>

void Main()
{	
	string urlWithAccessToken = "https://hooks.slack.com/services/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";
//curl -X POST --data-urlencode 'payload={"channel": "#deployment", "username": "webhookbot", "text": "This is posted to #deployment and comes from a bot named webhookbot.", "icon_emoji": ":ghost:"}' https://hooks.slack.com/services/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
//	https://{your_account}.slack.com/services/hooks/incoming-webhook?token={your_access_token}";
	
	SlackClient client = new SlackClient(urlWithAccessToken);
	
	client.PostMessage(username: "webhookbot",
			   text: "Please Ignore!",
			   channel: "@johne",
			   IconEmoj: ":ghost:");
}

//A simple C# class to post messages to a Slack channel
//Note: This class uses the Newtonsoft Json.NET serializer available via NuGet
public class SlackClient
{
	private readonly Uri _uri;
	private readonly Encoding _encoding = new UTF8Encoding();
	
	public SlackClient(string urlWithAccessToken)
	{
		_uri = new Uri(urlWithAccessToken);
	}
	
	//Post a message using simple strings
	public void PostMessage(string text, string username = null, string channel = null, string IconEmoj = null)
	{
		Payload payload = new Payload()
		{
			Channel = channel,
			Username = username,
			Text = text,
			IconEmoji=IconEmoj
		};
		
		PostMessage(payload);
	}
	
	//Post a message using a Payload object
	public void PostMessage(Payload payload)
	{
		string payloadJson = JsonConvert.SerializeObject(payload);
		
		using (WebClient client = new WebClient())
		{
			NameValueCollection data = new NameValueCollection();
			data["payload"] = payloadJson;
	
			var response = client.UploadValues(_uri, "POST", data);
			
			//The response text is usually "ok"
			string responseText = _encoding.GetString(response);
		}
	}
}

//This class serializes into the Json payload required by Slack Incoming WebHooks
public class Payload
{
	[JsonProperty("channel")]
	public string Channel { get; set; }
	
	[JsonProperty("username")]
	public string Username { get; set; }
	
	[JsonProperty("text")]
	public string Text { get; set; }

	[JsonProperty("icon_emoji")]
	public string IconEmoji { get; set; }
}