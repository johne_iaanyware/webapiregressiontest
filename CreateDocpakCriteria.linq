<Query Kind="Program">
  <Connection>
    <ID>5a008b72-6ad8-4d79-ad59-44c7eaeca1d6</ID>
    <Server>10.0.0.31</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>AU_IAA_DEV</Database>
    <NoPluralization>true</NoPluralization>
    <NoCapitalization>true</NoCapitalization>
    <UserName>sa</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA8F6mNav8qE+DhMOMGurGlAAAAAACAAAAAAADZgAAwAAAABAAAABZTMFrkq0BmweUii3BEwmxAAAAAASAAACgAAAAEAAAAHRgoq9FotMe/ruqEO5W5pkQAAAAoJ/pKdeV5At9DjlukFpS+hQAAAA7VDyVkuZ3ViBaFqVPkYa45bTYSQ==</Password>
    <Persist>true</Persist>
  </Connection>
  <NuGetReference>Ia.Domain.Models</NuGetReference>
  <NuGetReference Version="1.0.15.3">Ia.Reports.Models</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Ia.Domain.Models.Report</Namespace>
</Query>

void Main()
{
	var dp = document_pack
	.Where(d => !d.document_pack_type.action_type_required && d.status_flag == "Y" && d.document.document_name != "Table of Contents"
	&& d.document_pack_type_id == 1)
	.Select(d => new
	{
		d.document_pack_type_id,
		document_pack_type = d.document_pack_type.Content,
		d.document.document_name,
		d.document.report_id,
		d.sequence_no,
		d.document.report
	})
	.GroupBy(d => d.document_pack_type + " \n" + d.document_pack_type_id)
	.Select(x => new
	{
		document_pack_type = x.Key,
		documents = x.Select(d => new
		{
			d.sequence_no,
			//d.document_name,
			file_name = string.Format("{0:00}_{1}", d.sequence_no, d.document_name.Replace(" ", "").Replace("-", "_")),
			d.report_id,
			//d.report.report_name
		}).Distinct().OrderBy(d => d.sequence_no)
	});

	var docs = dp.First().documents;

	//docs.Dump(); return;

	var filenames = docs.Select(d => d.file_name);
	jsonfi(filenames).Dump();
	//var rcList = new List<ReportCriteria>();
	foreach (var doc in docs)
	{
		File.WriteAllText(Path.Combine(jPath, doc.file_name + ".json"), jsonfi(CreateQuoteReportCriteria(doc)));
	}
}

private string jsonfi(object o)
{
	return Newtonsoft.Json.JsonConvert.SerializeObject(o);
}

private string jPath => @"D:\Code\UnitTestWebApi\TestResults\AU\client_quote_docpack\json";

private Dictionary<string, ReportCriteria> Criteria => new Dictionary<string, ReportCriteria>()
{
	["AUQUOTE"] = new ReportCriteria
	{
		ClientId = new int[] { 179 },
		QuoteOrPolicyVersionOrClaimIds = new[] { 601084277 }, //P601084277
		PolicyType = Ia.Domain.Enums.PolicyType.Quote,
		ReportCategory = Ia.Domain.Enums.ReportCategory.SingleDocument,
		CombinedDoc = false,
		SecurityUsersId = 600000553
	}
};

private ReportCriteria CreateQuoteReportCriteria(dynamic doc)
{
	return CreateCriteria(doc, "AUQUOTE");
}

private ReportCriteria CreateCriteria(dynamic doc, string key)
{
	return new ReportCriteria
	{
		ClientId = Criteria[key].ClientId,
		QuoteOrPolicyVersionOrClaimIds = Criteria[key].QuoteOrPolicyVersionOrClaimIds,
		PolicyType = Criteria[key].PolicyType,
		ReportCategory = Criteria[key].ReportCategory,
		CombinedDoc = Criteria[key].CombinedDoc,
		SecurityUsersId = Criteria[key].SecurityUsersId,
		ReportId = doc.report_id,
		ReportName = doc.file_name,
	};
}